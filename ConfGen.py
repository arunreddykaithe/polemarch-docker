import configparser
import os
import sys
import time
import traceback
from subprocess import check_call

# Create important directories
for directory in ['/projects', '/hooks']:
    if not os.path.exists(directory):
        os.makedirs(directory)

# SQLite prepearing
sqlite_default_dir = os.environ.get('POLEMARCH_SQLITE_DIR', '/')
if sqlite_default_dir != '/' and not os.path.exists(sqlite_default_dir):
    os.makedirs(sqlite_default_dir)
if sqlite_default_dir[-1] != '/':
    sqlite_default_dir += '/'
sqlite_default_name = os.environ.get('POLEMARCH_SQLITE_DBNAME', 'db.sqlite3')
sqlite_db_path = '{db_place}/{db_name}'.format(
    db_place=sqlite_default_dir,
    db_name=sqlite_default_name
)

# Start configuring config file
config = configparser.ConfigParser()

# Set log level
log_level = os.getenv('POLEMARCH_LOG_LEVEL', 'WARNING')
# Set default settings
config['main'] = {
    'debug': os.getenv('POLEMARCH_DEBUG', 'false'),
    'log_level': log_level,
    'timezone': os.getenv('POLEMARCH_TIMEZONE', 'UTC'),
    'projects_dir': '/projects',
    'hooks_dir': '/hooks',
    'enable_admin_panel': os.getenv('POLEMARCH_ENABLE_ADMIN_PANEL', 'false')
}
# ldap-server, ldap-default-domain if exist
ldap_server = os.getenv('POLEMARCH_LDAP_CONNECTION', None)
if ldap_server:
    config['main']['ldap_server'] = ldap_server
ldap_default_domain = os.getenv('POLEMARCH_LDAP_DOMAIN', None)
if ldap_default_domain:
    config['main']['ldap-default-domain'] = ldap_default_domain


# Set db config
if os.getenv('POLEMARCH_DB_HOST') != None:
    try:
        pm_type = os.getenv('POLEMARCH_DB_TYPE', 'mysql')
        default_port = ''
        config['database.options'] = {
            'connect_timeout': os.getenv('POLEMARCH_DB_CONNECT_TIMEOUT', '20'),
        }
        if pm_type == 'mysql':
            default_port = '3306'
            config['database.options']['init_command'] = os.getenv('DB_INIT_CMD', '')
        elif pm_type == 'postgresql':
            default_port = '5432'
        config['database'] = {
            'engine': 'django.db.backends.{}'.format(pm_type),
            'name': os.environ['POLEMARCH_DB_NAME'],
            'user': os.environ['POLEMARCH_DB_USER'],
            'password': os.environ['POLEMARCH_DB_PASSWORD'],
            'host': os.environ['POLEMARCH_DB_HOST'],
            'port': os.getenv('POLEMARCH_DB_PORT', default_port),
        }
    except KeyError:
        raise Exception('Not enough variables for connect to  SQL server.')
else:
    config['database'] = {
        'engine': 'django.db.backends.sqlite3',
        'name': sqlite_db_path
    }

# Set cache and locks config
cache_loc = os.getenv('CACHE_LOCATION', '/tmp/polemarch_django_cache')
cache_type = os.getenv('POLEMARCH_CACHE_TYPE', 'file')
if cache_type == 'file':
    cache_engine = 'django.core.cache.backends.filebased.FileBasedCache'
elif cache_type == 'memcache':
    cache_engine = 'django.core.cache.backends.memcached.MemcachedCache'
elif cache_type == 'redis':
    cache_engine = 'django_redis.cache.RedisCache'

config['cache'] = config['locks'] = {
    'backend': cache_engine,
    'location': cache_loc
}

# Set rpc settings
rpc_connection = os.getenv('RPC_ENGINE', None)
config['rpc'] = {
    'heartbeat': os.getenv('RPC_HEARTBEAT', '5'),
    'concurrency': os.getenv('RPC_CONCURRENCY', '4'),
    'clone_retry_count': os.getenv('RPC_CLONE_RETRY_COUNT', '3')
}
if rpc_connection:
    config['rpc']['connection'] = rpc_connection

# Set web server and API settings
config['web'] = {
    'session_timeout': os.getenv('POLEMARCH_SESSION_TIMEOUT', '2w'),
    'rest_page_limit': os.getenv('POLEMARCH_WEB_REST_PAGE_LIMIT', '100'),
    'enable_gravatar': os.getenv('POLEMARCH_WEB_GRAVATAR', 'true')
}

config['uwsgi'] = {
    'processes': os.getenv('POLEMARCH_UWSGI_PROCESSES', '4'),
    'threads': os.getenv('POLEMARCH_UWSGI_THREADS', '4'),
    'thread-stacksize': os.getenv('POLEMARCH_UWSGI_THREADSTACK', '40960'),
    'max-requests': os.getenv('POLEMARCH_UWSGI_MAXREQUESTS', '50000'),
    'limit-as': os.getenv('POLEMARCH_UWSGI_LIMITS', '512'),
    'harakiri': os.getenv('POLEMARCH_UWSGI_HARAKIRI', '120'),
    'vacuum': os.getenv('POLEMARCH_UWSGI_VACUUM', 'true'),
    'pidfile': '/run/web.pid',
    'daemon': 'false'
}

# Set worker settings
config['rpc']['enable_worker'] = 'false'
if os.environ['WORKER'] == 'ENABLE':
    config['rpc']['enable_worker'] = 'true'
    config['worker'] = {
        'loglevel': log_level,
        'logfile': '/tmp/polemarch_worker.log',
        'pidfile': '/run/worker.pid',
        'beat': os.getenv('POLEMARCH_SCHEDULER_ENABLE', 'true')
    }

# Set secret key
if os.environ['SECRET_KEY'] != 'DISABLE':
    with open('/etc/polemarch/secret', 'w') as secretfile:
        secretfile.write(os.environ['SECRET_KEY'])

# Write config file
with open('/etc/polemarch/settings.ini', 'w') as configfile:
    config.write(configfile)

success = False
for i in range(60):
    try:
        check_call([sys.executable, '-m', 'polemarch', 'migrate'])
    except:
        time.sleep(1)
        print(traceback.format_exc())
        print("Retry #{}...".format(i))
    else:
        success = True
        break
if success:
    check_call([sys.executable, '-m', 'polemarch', 'webserver'])
else:
    sys.exit(10)
