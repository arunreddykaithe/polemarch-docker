FROM alpine:3.8
MAINTAINER sergey.k@vstconsulting.net

ARG  POLEMARCH_VERSION

ENV SECRET_KEY='DISABLE'
ENV WORKER='ENABLE'

COPY requirements.txt /etc/requirements.txt
COPY requirements-pip.txt /etc/requirements-pip.txt
COPY requirements-build.txt /etc/requirements-build.txt
ADD ./ConfGen.py /confgen.py

RUN cat /etc/requirements.txt | xargs apk --update add && \
    cat /etc/requirements-build.txt | xargs apk add --virtual .build-deps && \
    virtualenv -p python3 /opt/polemarch && \
    /opt/polemarch/bin/pip install -U pip wheel setuptools && \
    /opt/polemarch/bin/pip install -U -r /etc/requirements-pip.txt && \
    mkdir -p /projects && \
    mkdir -p /hooks && \
    mkdir -p /etc/polemarch && \
    /opt/polemarch/bin/pip install -U polemarch==${POLEMARCH_VERSION} && \
    mkdir -p /run/openldap && \
    chmod a+x /confgen.py && \
    apk --purge del .build-deps && \
    rm -rf ~/.cache/pip/* && \
    rm -rf /var/cache/apk/*


EXPOSE 8080

CMD [ "/opt/polemarch/bin/python", "/confgen.py"]
