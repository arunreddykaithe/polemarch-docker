# The main functions that Polemarch performs:
* execution templates;
* scheduled tasks execution;
* sharing of hosts, groups, inventories between projects;
* history of tasks execution with all details;
* easy configurable clustering for reliability and scalability:
* import of Ansible projects from Git repository (with submodules) or tar archive;
* import of inventory file;
* support of quick project deployment;
* documentation: http://polemarch.readthedocs.io/en/latest/ ;
* support of hosts groups and groups hierarchy;
* support of multi user connection;
* support of hooks;
* community project samples;
* user friendly interface.

## 1. Run in Docker

In order to start using the Polemarch container in Docker, you can follow the links 
and read the [detailed instructions](https://gitlab.com/vstconsulting/polemarch-docker/wikis/home)


## 2. Run in Kubernetes

To deploy Polemarch on your Kubernetes cluster, you can use the file **```kube_pm.yaml```**.

## Example of program deployment in kubernetes

To deploy the application, you can use two tools: ``make`` or ``kubectl``.

### 1. To deploy the Polemarch with the command ``make``
You can use the utility - make. Supported commands:
* ``check`` - a template of the configuration file will be displayed;
* ``deploy`` - a configuration file will be generated and sent to deploy the application on the kubernetes cluster;
* ``delete`` - the polemarch application previously deployed on your cluster will be deleted.

For convenience, variables such as:
* ``NAMESPACE_PM`` - the spatial name of the application;
* ``REPLICA_VOL`` - the number of replications of the Polemarch application;
* ``TAG_PM`` - version of the application Polemarch;
* ``IMAGE_PM`` - link to the DOCKER image of the Polemarch application;
* ``DOMAIN_NAME`` - is the domain name on which the deployed application will run.


### 2. To deploy the Polemarch with the command ``kubectl``

To deploy an application to your cluster, you need to enter the following command:
``$ kubectl apply -f ./kube_pm.yaml``

Also, when deploying an application, you can specify your own unique spatial name, for this you will need to enter the following command:
``$ kubectl apply --namespace = NAME -f ./kube_pm.yaml``
where ``NAME`` will be the spatial name of your application.

To remove the deployed application, you enter the following command:
``$ kubectl apply -f ./kube_pm.yaml`` or
``$ kubectl dole --namespace = NAME -f ./kube_pm.yaml``


### 3. You will get the result
After deployment, by default, you will receive :
* The latest version of MySQL with a reserved, permanent database storage;
* The latest version of Polemarch with a reserved, permanent file storage location used when working;
* Latest version of Redis for cache storage;
* Ingress, which will allow access from outside to the Polemarch web interface.
